CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------
The default link type fields do not allow content editors to select tag
attributes when creating content.

This module allows some attributes that may be important for SEO to be selected
via a field widget and a field formatter and can be introduced into an existing project without the need to create new fields or do data migrations.

The following attributes are currently supported:
* title
* rel: do follow/no-follow

REQUIREMENTS
------------
The module only requires the link module included in drupal core.

INSTALATION
-----------
Install as you would normally install a contributed Drupal module.
Visit https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------
The module creates a new field widget and a new field formatter for link fields.
The only requirement is set them in the form display and display modes.

MAINTAINERS
-----------
Current maintainers:
 * Alvaro J. Hurtado Villegas - https://www.drupal.org/u/alvar0hurtad0
