<?php

namespace Drupal\link_field_attributes\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\link\Plugin\Field\FieldWidget\LinkWidget;;

/**
 * Plugin implementation of the 'link_field_attributes' widget.
 *
 * @FieldWidget(
 *   id = "link_field_attributes",
 *   label = @Translation("Link field attributes"),
 *   field_types = {
 *     "link"
 *   }
 * )
 */
class LinkFieldAttributesWidget extends LinkWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\link\LinkItemInterface $item */
    $item = $items[$delta];

    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    $attributes = $item->options;
    $element['attributes'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Attributes'),
      '#attributes' => ['class' => ['link-field-widget-attributes']],
      'attribute_title' => [
        '#type' => 'textfield',
        '#title' => $this->t('Title'),
        '#default_value' => isset($attributes['attribute_title']) ? $attributes['attribute_title'] : NULL,
      ],
      'attribute_follow' => [
        '#type' => 'radios',
        '#title' => $this->t('Follow'),
        '#default_value' => isset($attributes['attribute_follow']) ? $attributes['attribute_follow'] : 'follow',
        '#options' => [
          'follow' => 'Do follow',
          'no-follow' => 'No follow',
        ],
      ],
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $element['attributes'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Attributes'),
      '#attributes' => ['class' => ['link-field-widget-attributes']],
      'attribute_title' => [
        '#type' => 'textfield',
        '#title' => $this->t('Title'),
        '#default_value' => $this->getSetting('attribute_title'),
      ],
      'attribute_follow' => [
        '#type' => 'select',
        '#title' => $this->t('Follow'),
        '#default_value' => $this->getSetting('attribute_follow'),
        '#options' => [
          'follow' => 'Do follow',
          'no-follow' => 'No follow',
        ],
      ],
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    foreach ($values as &$value) {
      $value['uri'] = static::getUserEnteredStringAsUri($value['uri']);
      $value['options'] = [
        'attribute_title' => $value['attributes']['attribute_title'],
        'attribute_follow' => $value['attributes']['attribute_follow'],
      ];
    }
    return $values;
  }

}
