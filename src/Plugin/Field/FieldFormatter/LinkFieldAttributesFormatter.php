<?php

namespace Drupal\link_field_attributes\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\link\Plugin\Field\FieldFormatter\LinkFormatter;

/**
 * Plugin implementation of the 'link_field_attributes' formatter.
 *
 * @FieldFormatter(
 *   id = "link_field_attributes",
 *   label = @Translation("Link field attributes"),
 *   field_types = {
 *     "link"
 *   }
 * )
 */
class LinkFieldAttributesFormatter extends LinkFormatter {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = parent::viewElements($items, $langcode);

    // Add the title attribute on each link.
    foreach ($element as $delta => $item) {
      $element[$delta]['#attributes'] = [
        'title' => $item['#options']['attribute_title'],
      ];

      if ($item['#options']['attribute_follow'] == 'no-follow') {
        $element[$delta]['#attributes']['rel'] = 'no-follow';
      }
    }
    return $element;
  }

}
